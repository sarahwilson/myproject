var app = angular.module('myProjectApp', []);

app.controller("BlogController", function($scope) {
  $scope.test_content = "Some test content from the Blog Controller";
  $scope.test_input = "";

  $scope.cars = [
    { make: "Volkswagen", model: "Polo", colour: "Blue", price: "1000.00", in_use: true },
    { make: "Vauxhall", model: "Astra", colour: "Red", price: "8000.00", in_use: true },
    { make: "Ford", model: "Ka", colour: "Silver", price: "1000.00", in_use: false},
    { make: "Renault", model: "Twingo", colour: "Blue", price: "6000.00", in_use: true }
  ]

  $scope.addNewCar = function(newCarDetails){
    $scope.cars.push(newCarDetails);
  }
});